package com.example.finnkkino;

public class Theatre {
    private int ID;
    private String name;

    public Theatre(int i, String s){
        name=s;
        ID=i;

    }
    public String getName(){
        return this.name;
    }
    public int getID(){
        return this.ID;
    }
    @Override
    public String toString() {
        return this.name; // What to display in the Spinner list.
    }
}

