package com.example.finnkkino;


import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    TextView StatusText;
    ArrayList<Theatre> Teatterit = new ArrayList<Theatre>();
    int selection;
    EditText Date;
    EditText ElokuvaNimi;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        StatusText = (TextView) findViewById(R.id.textView);
        Spinner spinner = (Spinner) findViewById(R.id.spinner1);

        readTxml();
            ArrayAdapter<Theatre> dataAdapter = new ArrayAdapter<Theatre>(this,
                    android.R.layout.simple_spinner_item, Teatterit);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
        spinner.setOnItemSelectedListener(this);
        Date = (EditText)findViewById(R.id.Date1);
        ElokuvaNimi = (EditText)findViewById(R.id.MovieName1);

    }


    public void readTxml(){
        ArrayList<String> poistettavat = new ArrayList<String>();
        poistettavat.add("Pääkaupunkiseutu");
        poistettavat.add("Espoo");
        poistettavat.add("Helsinki");
        poistettavat.add("Tampere");

        try {
            DocumentBuilder builder =DocumentBuilderFactory.newInstance().newDocumentBuilder();
            String URL = "https://www.finnkino.fi/xml/TheatreAreas";
            Document doc = builder.parse(URL);
            doc.getDocumentElement().normalize();
            System.out.println("Root element:"+ doc.getDocumentElement().getNodeName());

            NodeList nList = doc.getDocumentElement().getElementsByTagName("TheatreArea");
            for (int i=0; i< nList.getLength(); i++){
                Node node = nList.item(i);

                if(node.getNodeType()== Node.ELEMENT_NODE){
                    Element element = (Element) node;
                    String Name = element.getElementsByTagName("Name").item(0).getTextContent();
                    int ID = Integer.parseInt(element.getElementsByTagName("ID").item(0).getTextContent());
                    if(poistettavat.contains(Name)==false){
                        Teatterit.add(new Theatre(ID,Name));
                    }
                    StatusText.setText(ID+" "+Name);
                }

            }



        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
    }

    public void buttonFunc(View v){
        StatusText.setText("Button sussy ID: "+Teatterit.get(selection).getID());
    }
    public void onItemSelected(AdapterView<?> parent, View view, int pos,long id) {
        Toast.makeText(parent.getContext(),
                 ""+parent.getItemAtPosition(pos).toString(),
                Toast.LENGTH_SHORT).show();
        StatusText.setText(parent.getItemAtPosition(pos).toString());
        //StatusText.setText(parent.getItemAtPosition(pos).toString());
        selection = pos;

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public void readEventXml(View v){
        ArrayList<Element> Elokuvat = new ArrayList<Element>();
        StatusText.setText("https://www.finnkino.fi/xml/Schedule/?area="+Teatterit.get(selection).getID()+"?dt="+Date.getText().toString());
        try {
            String URL = "";
            DocumentBuilder builder =DocumentBuilderFactory.newInstance().newDocumentBuilder();
            if(Date.getText().toString().length() > 1){
                URL = "https://www.finnkino.fi/xml/Schedule/?area="+Teatterit.get(selection).getID()+"?dt="+Date.getText().toString();
            }else {
                URL = "https://www.finnkino.fi/xml/Schedule/?area="+Teatterit.get(selection).getID();
            }
            Document doc = builder.parse(URL);
            doc.getDocumentElement().normalize();
            System.out.println("Root element:"+ doc.getDocumentElement().getNodeName());
            String Displayed =Teatterit.get(selection).getID()+":\n";
            NodeList nList = doc.getDocumentElement().getElementsByTagName("Show");
            for (int i=0; i< nList.getLength(); i++){
                Node node = nList.item(i);

                if(node.getNodeType()== Node.ELEMENT_NODE){
                    Element element = (Element) node;
                    String Title = element.getElementsByTagName("Title").item(0).getTextContent();
                    String AlkAika = element.getElementsByTagName("dttmShowStart").item(0).getTextContent();
                    Elokuvat.add(element);
                    Displayed+=Elokuvat.get(i).getElementsByTagName("Title").item(0).getTextContent()+"\nAlkaa: "+AlkAika+"\n";


                    System.out.println(Title);

                }

            }


            StatusText.setText(Displayed);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }

    }
}
